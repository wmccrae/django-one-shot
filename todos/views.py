from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoForm, TodoItemForm


# This view shows the list of Todo Lists
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_details(request, id):
    requested_list = get_object_or_404(TodoList, id=id)
    context = {"list_object": requested_list}
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            newitem = form.save()
            return redirect("todo_list_detail", id=newitem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)


def edit_list(request, id):
    list_to_edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list_to_edit)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=list_to_edit)

    context = {
        "list_object": list_to_edit,
        "list_form": form,
    }
    return render(request, "todos/edit.html", context)


def edit_item(request, id):
    item_to_edit = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item_to_edit)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item_to_edit)

    context = {
        "form": form,
    }
    return render(request, "todos/edititem.html", context)


def delete_list(request, id):
    list_to_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_to_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
